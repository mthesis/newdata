import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm



fnqcd="jetsqcd.npz"
fnldm="jetsldm.npz"

def getq(q,p):#jets, jetdata
  qtheta=np.array([x[:,4] for x in q])
  qphi=np.array([x[:,5] for x in q])
  ptheta=p[:,0]
  pphi=p[:,1]
  dtheta=-(ptheta-qtheta)
  dphi=-(pphi-qphi)
  return np.concatenate(dtheta),np.concatenate(dphi)

def histpt():
  fqcd=np.load(fnqcd,allow_pickle=True)
  fldm=np.load(fnldm,allow_pickle=True)

  tqcd,pqcd=getq(fqcd["jets"],fqcd["jetdata"])#list of lists of True/False
  tldm,pldm=getq(fldm["jets"],fldm["jetdata"])
  
  #plt.hist(jqcd,bins=50,alpha=0.5,color="orange",label="qcd")
  #plt.hist(jldm,bins=50,alpha=0.5,color="blue",label="ldm")

  fix,(ax1,ax2)=plt.subplots(1,2,figsize=(12,6))

  ax1.hist(np.abs(tqcd),bins=50,alpha=0.5,color="orange",label="qcd",density=True)
  ax1.hist(np.abs(tldm),bins=50,alpha=0.5,color="blue",label="top",density=True)
  ax1.legend()
  ax1.set_title("theta")
  ax1.set_yscale("log",nonposy="clip")
  ax1.set_ylabel("#")
  ax1.set_xlabel("theta")

  
  ax2.hist(np.abs(pqcd),bins=50,alpha=0.5,color="orange",label="qcd",density=True)
  ax2.hist(np.abs(pldm),bins=50,alpha=0.5,color="blue",label="top",density=True)
  ax2.legend()
  ax2.set_title("phi")
  ax2.set_yscale("log",nonposy="clip")
  ax2.set_ylabel("#")
  ax2.set_xlabel("phi (except for minor simplifications)")



  plt.savefig("imgs/altangulardist.png",format="png")
  plt.savefig("imgs/altangulardist.pdf",format="pdf")


  plt.show()

  
histpt()












