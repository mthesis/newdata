import numpy as np
import matplotlib.pyplot as plt




fnqcd="jetsqcd.npz"
fnldm="jetsldm.npz"



def histpt():
  fqcd=np.load(fnqcd)
  fldm=np.load(fnldm)



  jqcd=fqcd["jetdata"][:,-1]
  jldm=fldm["jetdata"][:,-1]
  
  plt.hist(jqcd,bins=100,alpha=0.5,color="orange",label="qcd")
  plt.hist(jldm,bins=100,alpha=0.5,color="blue",label="ldm")

  plt.legend()

  plt.show()

  
histpt()












