import numpy as np
import sys

fn="full"

if len(sys.argv)>1:fn=sys.argv[1]


f=np.load(fn+".npz")

x=f["x"]
y=f["y"]
l=len(y)

train=0.6
val=0.20
test=0.20

id1=int(l*train)
id2=int(l*(train+val))


print("saving train")
np.savez_compressed(fn.replace("full","train"),x=x[:id1,:,:],y=y[:id1])

print("saving val")
np.savez_compressed(fn.replace("full","val"),x=x[id1:id2,:,:],y=y[id1:id2])

print("saving test")
np.savez_compressed(fn.replace("full","test"),x=x[id2:,:,:],y=y[id2:])





