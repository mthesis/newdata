import numpy as np

seed=12
np.random.seed(seed)

#3:new qcd
#4:new ldm

print("loading")
print("1")
f0=np.load("fqcd.npz")["q"]
print("2")
f1=np.load("fldm.npz")["q"]
print("done")

l0=f0.shape[0]
l1=f1.shape[0]

l=np.min([l0,l1])

print("cutting")
f0=f0[:l,:,:]
f1=f1[:l,:,:]


print("concatting")

f=np.concatenate((f0,f1),axis=0)
y0=np.ones(l)*3
y1=np.ones(l)*4
y=np.concatenate((y0,y1))

print("shuffling")
np.random.seed(12)
np.random.shuffle(f)
np.random.seed(12)
np.random.shuffle(y)

print("saving")
print("1")
np.savez_compressed("full",x=f,y=y)
print("2")
np.savez_compressed("full_au3",x=f0,y=y0)
print("3")
np.savez_compressed("full_au4",x=f1,y=y1)
print("done")








