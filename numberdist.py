import numpy as np
import matplotlib.pyplot as plt




fnqcd="jetsqcd.npz"
fnldm="jetsldm.npz"

def getl(q):
  return [len(x) for x in q]

def histpt():
  fqcd=np.load(fnqcd,allow_pickle=True)
  fldm=np.load(fnldm,allow_pickle=True)

  jqcd=getl(fqcd["jets"])#list of lists of True/False
  jldm=getl(fldm["jets"])
  
  plt.hist(jqcd,bins=50,alpha=0.5,color="orange",label="qcd")
  plt.hist(jldm,bins=50,alpha=0.5,color="blue",label="ldm")

  plt.legend()

  plt.savefig("imgs/numberdist.png",format="png")
  plt.savefig("imgs/numberdist.pdf",format="pdf")


  plt.show()

  
histpt()












