import numpy as np
import matplotlib.pyplot as plt

#the pt distributions for the max pt particle

index=0


f0=np.load("fqcd.npz")["q"]
f1=np.load("fldm.npz")["q"]


def getpt(q,i=0):
  return np.sqrt(q[:,i,1]**2+q[:,i,2]**2)


p0=getpt(f0,i=index)
p1=getpt(f1,i=index)


plt.hist(p0,bins=50,alpha=0.5,color="orange",label="qcd")
plt.hist(p1,bins=50,alpha=0.5,color="blue",label="ldm")


plt.legend()

plt.yscale("log",nonposy="clip")

plt.show()

