import numpy as np
import ROOT as r
import tqdm

r.gSystem.Load("libDelphes.so")



fn="/mount/vol1/scratch/work/kluettermann/t/bn227573/delphes_qvqvbar_mzp2000_etmiss_ptfatj100_1m.root"
fo="/mount/vol1/scratch/work/kluettermann/t/bn227573/jetsldm"


fn="/mount/vol1/scratch/work/kluettermann/t/bn227573/delphes_QCD_ptj100_14TeV_1M_0.root"
fo="/mount/vol1/scratch/work/kluettermann/t/bn227573/jetsqcd"






chain=r.TChain("Delphes")
fil=r.TFile(fn)
#chain.Add(fil.Delphes)


tr=r.ExRootTreeReader(fil.Delphes)

noe=tr.GetEntries()


bfjet=tr.UseBranch("FatJet")

branchEFlowTrack = tr.UseBranch("EFlowTrack")
branchEFlowPhoton = tr.UseBranch("EFlowPhoton")
branchEFlowNeutralHadron = tr.UseBranch("EFlowNeutralHadron")
branchParticle = tr.UseBranch("Particle")



jets=[]
#list of jet elements
#those are build out of e,p1,p2,p3,eta,phi,pt
jetdata=[]
#general information about the jet, so jetpt,jeteta,jetphi

#noe=1000


for entry in tqdm.tqdm(range(noe)):
    tr.ReadEntry(entry)
    njet=bfjet.GetEntriesFast()
    #if not (entry%100):print("working on",entry,"of",noe) 
    for i in range(njet):

        jet=bfjet.At(i)
        jeta=jet.Eta
        jphi=jet.Phi
        jpt=jet.PT
        jetdata.append(np.array([float(jeta),float(jphi),float(jpt)]))
        #print(jeta)

        cst=jet.Constituents

        toa=[]
        for j in range(cst.GetEntriesFast()):
            ac=cst.At(j)
            #print(dir(ac))
            #print(ac)
            #exit()
            p=ac.P4()
            #print(p.E(),p.Px(),p.Py(),p.Pz())
            try:
                toa.append(np.array([float(p.E()),float(p.Px()),float(p.Py()),float(p.Pz()),float(ac.Eta),float(ac.Phi),float(ac.PT)]))
            except:
                toa.append(np.array([float(p.E()),float(p.Px()),float(p.Py()),float(p.Pz()),float(ac.Eta),float(ac.Phi),float(ac.ET)]))
        jets.append(np.array(toa))
        #exit()

np.savez_compressed(fo,jets=np.array(jets),jetdata=np.array(jetdata))



print("did",noe)
exit()




fil=r.TFile(fn)

tree=fil.Delphes



for event in tree:
    for entry in event.GetEntries():
        print(dir(entry))
        print(entry)
        exit()

