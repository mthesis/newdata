import numpy as np
import sys


q="train"
n=10000


if len(sys.argv)>1:
  q=sys.argv[1]
if len(sys.argv)>2:
  n=int(sys.argv[2])


f=np.load(q+".npz",allow_pickle=True)

print("loaded")


x=f["x"][:n]
y=f["y"][:n]

print("rowed")

np.savez_compressed(q+"_fairer.npz",x=x,y=y)

print("saved")
