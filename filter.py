import numpy as np
import matplotlib.pyplot as plt

import sys


mode=0

if len(sys.argv)>1:
  mode=int(sys.argv[1])


fnqcd="jetsqcd.npz"
fnldm="jetsldm.npz"



fqcd=np.load(fnqcd,allow_pickle=True)
fldm=np.load(fnldm,allow_pickle=True)

if mode==0:
  print("loading j0")
  j0=fqcd["jets"]
  print("loading d0")
  d0=fqcd["jetdata"]
  print("loaded")
if mode==1:
  print("loading j1")
  j1=fldm["jets"]
  print("loading d1")
  d1=fldm["jetdata"]
  print("loaded")
#j1=fldm["jets"]
#d1=fldm["jetdata"]



def filter(j,d,nam):
  ret=[]

  print("starting filtering")

  maxpt=500
  minpt=100
  etacut=2.0
  dRsqmax=0.8**2
  n=len(j)

  for i,(aj,ad) in enumerate(zip(j,d)):
    if not (i%100):print("did",i,"of",n)
    acpt=ad[-1]
    aceta=ad[0]
    if acpt<minpt or acpt>maxpt:continue
    if np.abs(aceta)>etacut:continue
    dRsq=(aj[:,-3]-ad[0])**2+(aj[:,-2]-ad[1])**2
    
    aj=aj[np.where(dRsq<dRsqmax)]

    aj2=np.array(sorted(aj,key=lambda x: x[-1],reverse=True))

    aj3=np.pad(aj2[:,0:4],((0,200-aj2.shape[0]),(0,0)),constant_values=0)
    

    ret.append(aj3)
    #if i>1000:break
  print("saving....",nam)
  np.savez_compressed(nam,q=np.array(ret))
  print("done")
    




if mode==0:filter(j0,d0,"fqcd")
if mode==1:filter(j1,d1,"fldm")







