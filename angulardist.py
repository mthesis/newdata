import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm



fnqcd="jetsqcd.npz"
fnldm="jetsldm.npz"

def getq(q,p):#jets, jetdata
  qtheta=np.array([x[:,4] for x in q])
  qphi=np.array([x[:,5] for x in q])
  ptheta=p[:,0]
  pphi=p[:,1]
  dtheta=-(ptheta-qtheta)
  dphi=-(pphi-qphi)
  return np.concatenate(dtheta),np.concatenate(dphi)

def histpt():
  fqcd=np.load(fnqcd,allow_pickle=True)
  fldm=np.load(fnldm,allow_pickle=True)

  tqcd,pqcd=getq(fqcd["jets"],fqcd["jetdata"])#list of lists of True/False
  tldm,pldm=getq(fldm["jets"],fldm["jetdata"])
  
  #plt.hist(jqcd,bins=50,alpha=0.5,color="orange",label="qcd")
  #plt.hist(jldm,bins=50,alpha=0.5,color="blue",label="ldm")

  fix,(ax1,ax2)=plt.subplots(1,2,figsize=(12,6))


  ax1.hist2d(tqcd,pqcd,bins=20,range=[[-0.8,0.8],[-0.8,0.8]],density=True,cmin=0,norm=LogNorm())
  ax2.hist2d(tldm,pldm,bins=20,range=[[-0.8,0.8],[-0.8,0.8]],density=True,cmin=0,norm=LogNorm())
  
  ax1.set_title("qcd")
  ax2.set_title("ldm")

  #plt.legend()

  plt.savefig("imgs/angulardist.png",format="png")
  plt.savefig("imgs/angulardist.pdf",format="pdf")


  plt.show()

  
histpt()












